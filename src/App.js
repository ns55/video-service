import React, { Fragment } from 'react';
import './App.css';
import logo from './interface/assets/img/logo.png'; 
import movieItem from './interface/components/movieItem/movieItem';

class App extends React.Component {
  render() {

  return (
    <Fragment>
      <header className="header">
        <div className="wrapper">
          <div className="header-nav">
          <img className="header__logotype" src={logo} alt="logo" />
          <div className="header__search">
            <input type="search" placeholder="Поиск..." />
            <button type="button">Найти</button>
          </div>
          <button className="header-nav__button" type="button">Войти</button>
          </div>
          <div className="header__tabs">
            <div className="tabs-wrapper">
              <button type="button" className="content__button active">Фильмы</button>
              <button type="button" className="content__button">Телеканалы</button>
            </div>
          </div>
        </div>
      </header>
      <div className="content">
        <div className="content__cards">
          <movieItem />
          <div>genres</div>
        </div>
      </div>
      </Fragment>
  );
}
}

export default App;
