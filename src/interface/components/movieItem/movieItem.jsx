import React from 'react';
import css from './movieItem.module.css';

const movieItem = (props) => {
  return (
    <>
      <ul className={css.moviesItemList}>
        { props.map((movie) => (
              <li className={css.movieItem} key={movie.id}>
                <img src={movie.img} alt="movieImg"/>
              </li>
            )) }
      </ul>
    </>
  );
};

export default movieItem;
